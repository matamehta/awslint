# Changelog

This project follows the [Semantic Versioning 2.0.0](https://semver.org/) convention.

## v2.3.0

* add drift detection as a build step prior to creation of a change set for CloudFormation stacks

## v2.2.0

* add support for cfn-python-lint

## v2.1.2

* allow estimate-template-cost to fail because of known issue with nested stacks that pass parameters
* show estimate-template-cost output in the screen

## v2.1.1

* replace all S3 URLs to use virtual host style addressing for regional parity

## v2.1.0

* add estimate_template_cost command

## v2.0.2

* resolve #25 by fixing support for `validate-templates` in us-east-1

## v2.0.1

* run cleanup jobs prior to delivery to avoid being blocked in [templates/.gitlab-ci-aws-cfn.yml](templates/.gitlab-ci-aws-cfn.yml)

## v2.0

* migrate cfn-build, cfn-test, cfn-deploy from their original separate projects to this project
* update [templates/.gitlab-ci-aws-cfn.yml](templates/.gitlab-ci-aws-cfn.yml) with new test stage definitions
* update README files for all modules

## v1.0

* initial version
