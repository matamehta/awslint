## Build for AWS Lint

## specify your base image pulled from docker hub
FROM scardon/ruby-node-alpine:2.5.1

## specify the group or individual maintaining image
LABEL maintainer="Mike Meluso, George Rolston"

RUN apk update && apk upgrade
RUN apk add --no-cache --update \
  python3 python3-dev \
  git jq zip unzip file bash

# pip3 needs to be run initialy to upgrade pip
RUN pip3 install --upgrade pip
RUN pip install boto3 \
  json-spec \
  yamllint \
  cfn-lint

RUN pip install awscli --upgrade

RUN npm install -g cfn-tail@latest && which cfn-tail

RUN gem install cfn-nag

RUN mkdir -p /usr/lib/cfn_utils

COPY cfn-build /usr/lib/cfn_utils/cfn-build
COPY cfn-test /usr/lib/cfn_utils/cfn-test
COPY cfn-deploy /usr/lib/cfn_utils/cfn-deploy
COPY cfn-budget /usr/lib/cfn_utils/cfn-budget

RUN find /usr/lib/cfn_utils/ -type f -iname "*.sh" -exec chmod a+x {} \;

RUN ln -s /usr/lib/cfn_utils/cfn-build/cleanup_bucket.sh /usr/bin/cleanup_bucket && chmod a+x /usr/bin/cleanup_bucket
RUN ln -s /usr/lib/cfn_utils/cfn-build/cleanup_failed_change_sets.sh /usr/bin/cleanup_failed_change_sets && chmod a+x /usr/bin/cleanup_failed_change_sets
RUN ln -s /usr/lib/cfn_utils/cfn-build/detect_drift.sh /usr/bin/detect_drift && chmod a+x /usr/bin/detect_drift
RUN ln -s /usr/lib/cfn_utils/cfn-build/package_lambda_simple.sh /usr/bin/package_lambda_simple && chmod a+x /usr/bin/package_lambda_simple
RUN ln -s /usr/lib/cfn_utils/cfn-build/stage_templates_s3.sh /usr/bin/stage_templates_s3 && chmod a+x /usr/bin/stage_templates_s3

RUN ln -s /usr/lib/cfn_utils/cfn-test/lint-templates.sh /usr/bin/lint-templates && chmod a+x /usr/bin/lint-templates
RUN ln -s /usr/lib/cfn_utils/cfn-test/validate-json.sh /usr/bin/validate-json && chmod a+x /usr/bin/validate-json
RUN ln -s /usr/lib/cfn_utils/cfn-test/validate-templates.sh /usr/bin/validate-templates && chmod a+x /usr/bin/validate-templates
RUN ln -s /usr/lib/cfn_utils/cfn-test/validate-yaml.sh /usr/bin/validate-yaml && chmod a+x /usr/bin/validate-yaml

RUN ln -s /usr/lib/cfn_utils/cfn-deploy/check_stack_final_status.sh /usr/bin/check_stack_final_status && chmod a+x /usr/bin/check_stack_final_status
RUN ln -s /usr/lib/cfn_utils/cfn-deploy/create_change_set.sh /usr/bin/create_change_set && chmod a+x /usr/bin/create_change_set
RUN ln -s /usr/lib/cfn_utils/cfn-deploy/create_describe_only_change_set.sh /usr/bin/create_describe_only_change_set && chmod a+x /usr/bin/create_describe_only_change_set
RUN ln -s /usr/lib/cfn_utils/cfn-deploy/delete_change_set.sh /usr/bin/delete_change_set && chmod a+x /usr/bin/delete_change_set
RUN ln -s /usr/lib/cfn_utils/cfn-deploy/delete_stack.sh /usr/bin/delete_stack && chmod a+x /usr/bin/delete_stack
RUN ln -s /usr/lib/cfn_utils/cfn-deploy/detect_changes.sh /usr/bin/detect_changes && chmod a+x /usr/bin/detect_changes
RUN ln -s /usr/lib/cfn_utils/cfn-deploy/execute_change_set.sh /usr/bin/execute_change_set && chmod a+x /usr/bin/execute_change_set
RUN ln -s /usr/lib/cfn_utils/cfn-deploy/get_stack_status.sh /usr/bin/get_stack_status && chmod a+x /usr/bin/get_stack_status

RUN ln -s /usr/lib/cfn_utils/cfn-budget/estimate_template_cost.sh /usr/bin/estimate_template_cost && chmod a+x /usr/bin/estimate_template_cost
