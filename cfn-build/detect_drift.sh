#!/bin/bash -e

## Required variables
# CFN_STACK_NAME

if [ -e cfn-deploy/get_stack_status.sh ]; then
  . cfn-deploy/get_stack_status.sh
else
  . /usr/bin/get_stack_status
fi

if get_stack_status
then
  case $stack_status in
    "CREATE_COMPLETE"|"UPDATE_COMPLETE"|"UPDATE_ROLLBACK_COMPLETE")
      { # try
        stackDriftDetectionId=$(aws cloudformation detect-stack-drift \
          --stack-name $CFN_STACK_NAME \
          --output text)
        detectionStatus="undefined"
        while [ "$detectionStatus" != "DETECTION_COMPLETE" ]; do
          detectionStatus=$(aws cloudformation describe-stack-drift-detection-status \
            --stack-drift-detection-id $stackDriftDetectionId \
            --query 'DetectionStatus' \
            --output text)
          if [ "$detectionStatus" == "DETECTION_FAILED" ]; then
            echo "Drift detection encountered an error."
            exit 1;
          fi
          sleep 10
        done
        aws cloudformation describe-stack-resource-drifts \
          --stack-name $CFN_STACK_NAME > drift.json
        cat drift.json
        if cat drift.json | jq -e 'any(.StackResourceDrifts[].StackResourceDriftStatus; contains("MODIFIED"))'
        then
          echo "Drift was detected."
          exit 1;
        else
          echo "No drift was detected."
          exit 0;
        fi
      } || { # catch
        echo "Drift detection ran into an error."
        exit 1;
      }
      ;;
    "UPDATE_ROLLBACK_FAILED" \
    |"ROLLBACK_COMPLETE"|"CREATE_FAILED"|"ROLLBACK_FAILED" \
    |"CREATE_IN_PROGRESS"|"DELETE_IN_PROGRESS"|"DELETE_IN_PROGRESS" \
    |"REVIEW_IN_PROGRESS"|"ROLLBACK_IN_PROGRESS"|"UPDATE_ROLLBACK_IN_PROGRESS" \
    |"UPDATE_ROLLBACK_COMPLETE_CLEANUP_IN_PROGRESS"|"UPDATE_IN_PROGRESS" \
    |"UPDATE_COMPLETE_CLEANUP_IN_PROGRESS"|"DELETE_FAILED")
      echo "Stack is not in a healthy/final state. Please restore the stack to CREATE_COMPLETE, UPDATE_COMPLETE, UPDATE_ROLLBACK_COMPLETE before attempting to detect drift."
      exit 1;
      ;;
    "DELETE_COMPLETE"|*)
      echo "Stack does not exist. No resource drift is possible."
      exit 0;
      ;;
  esac
else
  echo "Stack does not exist. No resource drift is possible."
  exit 0;
fi
