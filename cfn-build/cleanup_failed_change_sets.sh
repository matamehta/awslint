#!/bin/bash -e

get_change_set_status() {
	get_change_sets_command="aws cloudformation list-change-sets --stack-name $CFN_STACK_NAME --query 'Summaries[?ChangeSetName==\`$CFN_CHANGE_SET_NAME\`] | [0].Status'"
  change_set_status_raw=$(eval $get_change_sets_command)
  change_set_error=$?
  change_set_status=${change_set_status_raw//\"/}
  return $change_set_error
}

if get_change_set_status
then
  case $change_set_status in
    "CREATE_IN_PROGRESS" | "CREATE_COMPLETE")
      # we should not create the change set in these cases to avoid collision with an ongoing operation
      echo "Change set $CFN_CHANGE_SET_NAME already exists and is in status $change_set_status. Exiting..."
      exit 1
      ;;
    "FAILED")
      # when we have a change set with the same name and it is failed, we can safely delete it so this pipeline can recreate it
      echo "Change set $CFN_CHANGE_SET_NAME already exists and is in FAILED status. Cleaning up before Proceeding..."
      if aws cloudformation delete-change-set --stack-name $CFN_STACK_NAME --change-set-name $CFN_CHANGE_SET_NAME
      then
        echo "Change set was deleted."
      else
        echo "Change set could not be deleted."
        exit 1
      fi
      ;;
    "null")
      echo "No change set conflict present. Proceeding..."
      ;;
    *)
      # change set status is unknown
      echo "Change set $CFN_CHANGE_SET_NAME already exists and is in unknown status $change_set_status. Exiting..."
      exit 1
      ;;
  esac
else
  echo "Stack $CFN_STACK_NAME does not exist. No change sets to clean up."
  echo "No change set conflict present. Proceeding..."
fi
