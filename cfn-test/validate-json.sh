#!/bin/bash
ERROR_COUNT=0;

echo "Validating JSON files..."

# Loop through the JSON files in this repository
for JSON in $(find . -name '*.json'); do 

    # Validate the JSON with jq
    ERRORS=$(jq -e . $JSON 2>&1 >/dev/null); 
    if [ "$?" -gt "0" ]; then 
        ((ERROR_COUNT++));
        echo "[fail] $JSON: $ERRORS";
    else 
        echo "[pass] $JSON";
    fi; 
    
done; 

echo "$ERROR_COUNT json validation error(s)"; 
if [ "$ERROR_COUNT" -gt 0 ]; 
    then exit 1; 
fi
