#!/bin/bash -e

if [ -e cfn-deploy/check_stack_final_status.sh ]; then
  . cfn-deploy/check_stack_final_status.sh
else
  . /usr/bin/check_stack_final_status
fi

## Required variables
# CFN_STACK_NAME
# CFN_CHANGE_SET_NAME
## Options not implemented
# [--stack-name <value>]
# [--client-request-token <value>]
# [--cli-input-json <value>]
# [--generate-cli-skeleton <value>]

if aws cloudformation execute-change-set --stack-name $CFN_STACK_NAME --change-set-name $CFN_CHANGE_SET_NAME 
then
  cfn-tail $CFN_STACK_NAME
  check_stack_final_status
  case $stack_status in
    "CREATE_COMPLETE"|"UPDATE_COMPLETE")
      echo "The stack operation completed successfully."
      ;;
    *)
      echo "The stack operation was not completed. Stack reports status: $stack_status."
      exit 1
      ;;
  esac
else
  echo "Change set did not execute properly."
  exit 1
fi
