# CloudFormation Deployment Project

Contains scripts for performing CloudFormation stack creation and update in the `deploy` phase based on the current state of the stack. Also contains scripts useful for performing change management, `test`ing, and staging prior to a deployment.

## Software Dependencies

1. awscli
2. [cfn-tail](https://github.com/taimos/cfn-tail)

## Scripts

Scripts are categorized with a type and a stage. The definitions are below:

### Types

* **Type:** CI: this script is primarily used to execute CI jobs
* **Type:** Utility: this script is used by one or multiple CI or Utility scripts as a way to reuse and modularize code segments
* **Type:** CI/Utility: this script defines a function which can be used as a utility by other scripts and also has use cases as a standalone CI job

For use of these scripts outside of the `awslint` package: since Utility scripts define a function, the script must be sourced (`. ./script_name.sh`) so the function name is available to the bash session, and then the function name can be called to execute (`script_name`).

### Stages

`CI` or `CI/Utility` script types are designed to be used in one or more stages:

* `integration test` - jobs designed to test the combined functionality of CloudFormation templates and other resources
* `staging` - jobs designed to make CloudFormation templates and other deployment artifacts available for deployment ("Continuous Delivery")
* `deploy` - jobs designed to make changes to deploy a new CloudFormation stack or perform in-place updates on an existing stack ("Continuous Deployment")
* `cleanup` - jobs designed to eliminate any leftover resources from prior stages post-deployment

### List of Scripts

#### [check_stack_final_status.sh](check_stack_final_status.sh)

Checks if the stack is in a state which designates a final state; exits with an error if the state is not an end state or if the state is unknown.
**Type:** Utility

##### Required Parameters

* CFN_STACK_NAME

#### [create_change_set.sh](create_change_set.sh)

Creates and describes a change set. Can be used to update an existing stack or to create a new stack. The create or update decision will be based on the CFN_STACK_NAME.
**Type:** CI
**Stage:** `staging`

##### Required Parameters

* CFN_BASE_TEMPLATE
* CFN_STACK_NAME

##### Optional Parameters

* CFN_CHANGE_SET_NAME
* CFN_ROLE_ARN
* CFN_CAPABILITIES
* CFN_CHANGE_SET_DESCRIPTION
* CFN_PARAMETER_*

#### [create_describe_only_change_set.sh](create_describe_only_change_set.sh)

Creates, describes, and then deletes a change set. The main use case for this script is to describe changes made to nested stacks since these changes will not show as a part of the main template's change set. If using this script in that case, it is recommended to define a `describe_only_change_set` job for each nested template, each with their own variables defined per-job. Variables at the job level will override variables defined in a global `variables` block within [.gitlab-ci-yml](.gitlab-ci-yml).
**Type:** CI
**Stage:** `unit test` or `integration test` depending on whether you test on a single stack or multiple nested stacks

##### Required Parameters

* CFN_BASE_TEMPLATE
* CFN_STACK_NAME

##### Optional Parameters

* CFN_CHANGE_SET_NAME
* CFN_ROLE_ARN
* CFN_CAPABILITIES
* CFN_CHANGE_SET_DESCRIPTION
* CFN_PARAMETER_*

#### [delete_change_set.sh](delete_change_set.sh)

Deletes a change set; useful for cleaning up after evaluating a change set if the change set will not be executed in this pipeline
**Type:** CI/Utility
**Stage:** `cleanup`

##### Required Parameters

* CFN_STACK_NAME

##### Optional Parameters

* CFN_CHANGE_SET_NAME

#### [delete_stack.sh](delete_stack.sh)

Deletes a stack; sets termination protection to false before deletion. **Use with caution** as this operation cannot be interrupted or undone.
**Type:** CI/Utility
**Stage:** `cleanup`

#### Required Parameters

* CFN_STACK_NAME

#### [detect_changes.sh](detect_changes.sh)

Detects whether a change set comes back with resource changes and exits with error if it does. This is best used with GitLab CI's `allow_failure: true` property for jobs so that they stand out as a warning but do not interrupt the CI pipeline (because the changes may be expected).
**Type:** Utility

##### Required Parameters

* CFN_CHANGE_SET_NAME
* CFN_STACK_NAME

#### [execute_change_set.sh](execute_change_set.sh)

Executes a previously-created change set, providing console output of the stack events until some end state is reached.
**Type:** CI/Utility
**Stage:** `deploy`

#### Required Parameters

* CFN_STACK_NAME

##### Optional Parameters

* CFN_CHANGE_SET_NAME

#### [get_stack_status.sh](get_stack_status.sh)

Gets the current status of the stack at this point in time, including if the stack has been deleted.
**Type:** Utility

#### Required Parameters

* CFN_STACK_NAME

## Parameters

We recommend defining these either in your .gitlab-ci.yml as `variables` or as project-level [secret variables](https://docs.gitlab.com/ee/ci/variables/#variables). The main requirement is that the below variables are available to the jobs requiring them. The "required by" and "used by" designate the minimum job set where the variables need to be available.

### Required

1. CFN_BASE_TEMPLATE
   * **Required by:** [create_change_set.sh](create_change_set.sh), [create_describe_only_change_set.sh](create_describe_only_change_set.sh)
   * **Description:** path to main template in the form of filename or template url beginning with `https://BUCKETNAME.s3`
2. CFN_STACK_NAME
   * **Required by:** all jobs
   * **Description:** the CloudFormation stack name to assign or the name of an existing stack

### Optional

1. CFN_CHANGE_SET_NAME
   * **Used by:** [create_change_set.sh](create_change_set.sh), [create_describe_only_change_set.sh](create_describe_only_change_set.sh), [delete_change_set.sh](delete_change_set.sh)
   * A generic name will be chosen if this parameter is not set
2. CFN_ROLE_ARN
   * **Used by:** [create_change_set.sh](create_change_set.sh), [create_describe_only_change_set.sh](create_describe_only_change_set.sh)
3. CFN_CAPABILITIES
   * **Used by:** [create_change_set.sh](create_change_set.sh), [create_describe_only_change_set.sh](create_describe_only_change_set.sh)
4. CFN_CHANGE_SET_DESCRIPTION
   * **Used by:** [create_change_set.sh](create_change_set.sh), [create_describe_only_change_set.sh](create_describe_only_change_set.sh)
5. CFN_PARAMETER_*
   * **Used by:** [create_change_set.sh](create_change_set.sh), [create_describe_only_change_set.sh](create_describe_only_change_set.sh)
   * **Description:** Specify CFN_PROCESSED_PARAMS variable or this value. These values will take precedence over CFN_PROCESSED_PARAMS.
   * **Syntax:** `CFN_PARAMETER_ParameterKey: ParameterValue`
6. CFN_PROCESSED_PARAMS
   * **Used by:** [create_change_set.sh](create_change_set.sh), [create_describe_only_change_set.sh](create_describe_only_change_set.sh)
   * **Description:** Specify a series of CFN_PARAMETER_* variables or this value
   * [AWS CloudFormation parameter reference](https://docs.aws.amazon.com/AWSCloudFormation/latest/APIReference/API_Parameter.html)
   * [AWS CloudFormation change set documentation](https://docs.aws.amazon.com/cli/latest/reference/cloudformation/create-change-set.html)

## Optional files

These files can be used to supply additional options to the cfn-deploy scripts:

1. cfn_tags.json
   * **Used by:** [create_change_set.sh](create_change_set.sh), [create_describe_only_change_set.sh](create_describe_only_change_set.sh)
2. cfn_rollback_configuration.json
   * **Used by:** [create_change_set.sh](create_change_set.sh), [create_describe_only_change_set.sh](create_describe_only_change_set.sh)
3. cfn_resource_types.txt
   * **Used by:** [create_change_set.sh](create_change_set.sh), [create_describe_only_change_set.sh](create_describe_only_change_set.sh)
