#!/bin/bash -e

if [ -e cfn-deploy/delete_stack.sh ]; then
  . cfn-deploy/delete_stack.sh
  . cfn-deploy/get_stack_status.sh
else
  . /usr/bin/delete_stack
  . /usr/bin/get_stack_status
fi

delete_change_set() {
  echo "Deleting the change set $CFN_CHANGE_SET_NAME from $CFN_STACK_NAME..."
  aws cloudformation delete-change-set --stack-name $CFN_STACK_NAME --change-set-name $CFN_CHANGE_SET_NAME
  echo "Change set deleted."
  if get_stack_status; then
    case $stack_status in
      "REVIEW_IN_PROGRESS")
        # delete REVIEW_IN_PROGRESS stacks
        echo "$CFN_STACK_NAME will be deleted since it is in the REVIEW_IN_PROGRESS state."
        delete_stack
        ;;
      *)
        # do nothing
        ;;
    esac
  fi
}
